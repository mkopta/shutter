int led = 8;
int sensor = 2;
unsigned long int opened_at = 0;
unsigned long int closed_at = 0;
double delta = 0;
bool is_open = false;
bool firing = false;


void setup() {
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);
  attachInterrupt(digitalPinToInterrupt(sensor), change, CHANGE);
  Serial.begin(9600);
  while (!Serial) {
    ;
  }
  Serial.println("start");
}

void loop() {
  if (is_open and not firing) {
    opened_at = micros();
    is_open = true;
    firing = true;
  } else if (not is_open and firing) {
    closed_at = micros();
    is_open = false;
    firing = false;
    delta = (closed_at - opened_at) / 1000.0;
    Serial.print("t = 1 / ");
    Serial.print(((double)1.0 / delta) * 1000.0);
    Serial.print(" (");
    Serial.print(delta / (double)1000.0);
    Serial.print(" s ; ");
    Serial.print(delta);
    Serial.print(" ms ; ");
    Serial.print(closed_at - opened_at);
    Serial.println(" us)");
    delay(500);
  }
}

void change() {
  if (digitalRead(sensor) == LOW){
    is_open = true;
  } else {
    is_open = false;
  }
}
